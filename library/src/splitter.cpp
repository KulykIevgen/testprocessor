#include <helper.h>
#include <splitter.h>

#include <algorithm>
#include <iterator>
#include <map>
#include <string>
#include <tuple>

splitter::splitter(const std::string& text)
{
    enum class States{Space,Symbols};
    States state = States::Space;
    auto start = text.cbegin();

    for (auto it = text.cbegin();it != text.cend();++it)
    {
        if (*it == ' ')
        {
            if (state == States::Symbols)
            {
                words.emplace_back(start,it);
            }
            state = States::Space;
        }
        else
        {
            if (state == States::Space)
            {
                start = it;
            }
            state = States::Symbols;
        }
    }

    if (state == States::Symbols)
    {
        words.emplace_back(start,text.cend());
    }
}

uint32_t splitter::get_words_number() const noexcept
{
    return words.size();
}

std::string splitter::get_longest_word() const noexcept
{
    using CSTR = const std::string&;
    auto result = help::max_element(words.cbegin(),words.cend(),[](CSTR a,CSTR b) -> bool
    {
        return a.length() < b.length();
    });
    return *result;
}

std::tuple<std::string, uint32_t> splitter::get_word_with_longet_seq() const
{
    using T = std::tuple<uint32_t,char>;
    using LINK = const T&;
    std::vector<T> word_info;

    for (const auto& word:words)
    {
        word_info.emplace_back(help::find_longest_seq(word.cbegin(),word.cend()));
    }

    auto maximum = help::max_element(word_info.cbegin(),word_info.cend(),[](LINK a,LINK b) -> bool
    {
        return std::get<0>(a) < std::get<0>(b);
    });

    auto number = std::distance(word_info.cbegin(),maximum);
    return std::make_tuple(words[number],std::get<0>(word_info[number]));
}

std::vector<std::tuple<std::string, uint32_t>> splitter::get_occurences_of_words() const
{
    std::map<std::string,uint32_t> container;
    std::vector<std::tuple<std::string,uint32_t>> result;

    for (const auto& word:words)
    {
        ++container[word];
    }

    for (const auto& element:container)
    {
        result.emplace_back(std::make_tuple(element.first,element.second));
    }

    return result;
}
