#include <helper.h>
#include <library.h>
#include <splitter.h>

#include <algorithm>
#include <climits>
#include <string>

uint32_t get_words_number(const std::string& text)
{
    splitter counter{text};
    return counter.get_words_number();
}

std::string get_longest_word(const std::string& text)
{
    splitter counter{text};
    return counter.get_longest_word();
}

std::tuple<std::string,uint32_t> get_max_repeatable_seq(const std::string& text)
{
    splitter counter{text};
    return counter.get_word_with_longet_seq();
}

std::string reverse_word(const std::string& word)
{
    auto copy = word;
    help::reverse(copy.begin(),copy.end());
    return std::move(copy);
}

std::vector<std::tuple<std::string, uint32_t>> get_occurences_of_words(const std::string &text)
{
    splitter counter{text};
    return counter.get_occurences_of_words();
}
