#ifndef TEXTPROCESSOR_SPLITTER_H
#define TEXTPROCESSOR_SPLITTER_H

#include <cstdint>
#include <vector>
#include <string>
#include <tuple>

/*!
 * Help class for processing strings with the text
 */
class splitter
{
public:
    /*!
     * splits the text and saves the words in vector
     * @param text the whole text
     */
    explicit splitter(const std::string& text);

    /*!
     * get number of splitted words after the text was processed
     * @return number of separate words
     */
    uint32_t get_words_number() const noexcept ;

    /*!
     * searchs for the longest word in the text
     * @return the longest word in text
     */
    std::string get_longest_word() const noexcept;

    /*!
     * Get one word from the text with the longest unbreakable sequance of repeatable chars
     * @return the pair in tuple of word and number of repeatable symbols in sequence
     */
    std::tuple<std::string,uint32_t> get_word_with_longet_seq() const;

    /*!
     * calculates how many times each word can be met in text
     * @return pairs (not std::pair) of words and occurences
     */
    std::vector<std::tuple<std::string, uint32_t>> get_occurences_of_words() const;

    splitter() = delete;
    splitter(const splitter&) = delete;
    splitter(splitter&&) = delete;
    splitter& operator=(const splitter&) = delete;
    splitter& operator=(splitter&&) = delete;
    ~splitter() = default;

private:
    std::vector<std::string> words;
};

#endif //TEXTPROCESSOR_SPLITTER_H
