#ifndef TEXTPROCESSOR_HELPER_H
#define TEXTPROCESSOR_HELPER_H

#include <cstdint>
#include <iterator>
#include <tuple>
#include <unordered_map>
#include <utility>

namespace help
{
    /*!
     * Gets the iterator of max element in container
     * @tparam T iterator type
     * @tparam U comparison predicate type
     * @param first begin iterator
     * @param last end iterator
     * @param predicate callable object to compare elements with <
     * @return iterator to max element
     */
    template<typename T,typename U>
    T max_element(T first,T last,U predicate)
    {
        T result = first++;

        while (first != last)
        {
            if (predicate(*result,*first))
            {
                result = first;
            }
            ++first;
        }

        return result;
    };
    /*!
     * Changes the order of elements in container to vise versa
     * @tparam T Random access iterator
     * @param start iterator, that points to the begining of the container
     * @param finish iterator, that points after the last element in container
     */
    template<typename T>
    void reverse(T start, T finish)
    {
        T last = std::prev(finish);
        while (start < last)
        {
            std::swap(*start++, *last--);
        }
    }

    /*!
     * Finds, what element in container makes the longest sequence without breaks
     * Break is input, that is different from current element
     * @tparam T iterator of the container
     * @param start begin iterator
     * @param finish end iterator
     * @return return tuple of length of sequence and value of element
     */
    template<typename T>
    std::tuple<typename T::value_type,uint32_t>
            find_longest_seq(T start,T finish)
    {
        using U = typename T::value_type;
        enum class State {None,Detect};
        State state = State::None;
        std::unordered_map<uint32_t,U> info;
        info[1] = *start;
        uint32_t count = 0;

        for (T i = std::next(start);i != finish;++i)
        {
            T j = std::prev(i);

            if (*j == *i)
            {
                if (state == State::None)
                {
                    count = 2;
                    state = State::Detect;
                }
                else if (state == State::Detect)
                {
                    ++count;
                }
                info[count] = *i;
            }
            else
            {
                state = State::None;
            }
        }

        uint32_t maximum = 0;
        U result = *start;
        for (const auto& value:info)
        {
            if (value.first > maximum)
            {
                maximum = value.first;
                result = value.second;
            }
        }
        return std::make_tuple(maximum,result);
    };

}

#endif //TEXTPROCESSOR_HELPER_H
