#ifndef TEXTPROCESSOR_LIBRARY_H
#define TEXTPROCESSOR_LIBRARY_H

#include <cstdint>
#include <string>

/*!
 * counts the number of words separated with space in the input string
 * @param text the whole input text
 * @return the number of words, could not be less then 0
 */
uint32_t get_words_number(const std::string& text);

/*!
 * finds out what word in the input text the longest
 * @param text the whole input text
 * @return the longest word from the text
 */
std::string get_longest_word(const std::string& text);

/*!
 * finds the word and the length of repeated chars in the word from text
 * @param text the whole input text
 * @return the word and the length
 */
std::tuple<std::string,uint32_t> get_max_repeatable_seq(const std::string &text);

/*!
 * reverse the input word
 * @param word one separate word
 * @return reversed word
 */
std::string reverse_word(const std::string& word);

/*!
 * calculates how many times each word can be met in text
 * @param text the whole input text
 * @return pairs (not std::pair) of words and occurences
 */
std::vector<std::tuple<std::string,uint32_t>> get_occurences_of_words(const std::string& text);

#endif