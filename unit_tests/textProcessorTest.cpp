#include "textProcessorTest.h"

#include <helper.h>
#include <library.h>

#include <thread>
#include <vector>

void textProcessorTest::TestGtestLibLink()
{
    EXPECT_EQ(1,1);
}

void textProcessorTest::TestWordsCalculation()
{
    std::string test("wellcome to the world");
    EXPECT_EQ(get_words_number(test),4u);
    std::string test_with_spaces(" wellcome to the world ");
    EXPECT_EQ(get_words_number(test_with_spaces),4u);
}

void textProcessorTest::TestFindLongertWord()
{
    std::string test("wellcome to the world");
    auto longest = get_longest_word(test);
    EXPECT_EQ(longest,std::string("wellcome"));
}

void textProcessorTest::TestFindMaxRepeat()
{
    std::string test("acccccbb hello twoo");
    auto number = get_max_repeatable_seq(test);
    EXPECT_EQ(std::get<0>(number),"acccccbb");
    EXPECT_EQ(std::get<1>(number),5u);
}

void textProcessorTest::TestReverseWord()
{
    std::string test("test");
    std::string tset("tset");
    EXPECT_EQ(tset,reverse_word(test));
}

void textProcessorTest::TestMultithreadedExample()
{
    std::string test("I want to be calculated in test");
    uint32_t number_of_iterations = 1000;
    const uint32_t thread_number = std::thread::hardware_concurrency();

    for (decltype(number_of_iterations) i = 0;i < number_of_iterations;i++)
    {
        std::vector<uint32_t> output(thread_number);
        std::vector<std::thread> workers;

        for (uint32_t j = 0;j < thread_number;j++)
        {
            workers.push_back(
                    std::thread(
                    [&test](uint32_t& result)
                    {
                        result = get_words_number(test);
                    },
                    std::ref(output[j])
                    )
            );
        }

        for (uint32_t j = 0;j < thread_number;j++)
        {
            workers[j].join();
        }

        for (uint32_t j = 0;j < thread_number;j++)
        {
            EXPECT_EQ(output[j],7u);
        }
    }
}

void textProcessorTest::TestHelpReverse()
{
    std::string abc("abc");
    std::string cba("cba");
    help::reverse(abc.begin(),abc.end());

    EXPECT_EQ(abc,cba);
}

void textProcessorTest::TestHelpFindSeq()
{
    std::string word("aabbbcccc");
    auto [length,value] = help::find_longest_seq(word.cbegin(),word.cend());
    EXPECT_EQ(length,4u);
    EXPECT_EQ(value,'c');
}

void textProcessorTest::TestOccurences()
{
    std::string text("is is is cool cool");
    auto scan_result = get_occurences_of_words(text);
    auto [word,occurence] = scan_result[0];
    if (word == "is")
    {
        EXPECT_EQ(occurence,3u);
    }
    else if (word == "cool")
    {
        EXPECT_EQ(occurence,2u);
    }
    else
    {
        FAIL() << word << " is unknown";
    }
}

void textProcessorTest::TestMaxElement()
{
    std::vector<int> a{1,2,4,5,6,7,8,22,20,17,15};
    auto result = help::max_element(a.cbegin(),a.cend(),[](int i,int j) -> bool
    {
       return i < j;
    });
    EXPECT_EQ(*result,22);
}
