#ifndef TEXTPROCESSOR_TEXTPROCESSORTEST_H
#define TEXTPROCESSOR_TEXTPROCESSORTEST_H

#include "gtest/gtest.h"

class textProcessorTest
{
public:
    static void TestGtestLibLink();
    static void TestWordsCalculation();
    static void TestFindLongertWord();
    static void TestFindMaxRepeat();
    static void TestReverseWord();
    static void TestMultithreadedExample();
    static void TestOccurences();

    static void TestHelpReverse();
    static void TestHelpFindSeq();
    static void TestMaxElement();
};

TEST(textProcessorTest,TestGtestLibLink)
{
    textProcessorTest::TestGtestLibLink();
}

TEST(textProcessorTest,TestWordsCalculation)
{
    textProcessorTest::TestWordsCalculation();
}

TEST(textProcessorTest,TestFindLongertWord)
{
    textProcessorTest::TestFindLongertWord();
}

TEST(textProcessorTest,TestFindMaxRepeat)
{
    textProcessorTest::TestFindMaxRepeat();
}

TEST(textProcessorTest,TestReverseWord)
{
    textProcessorTest::TestReverseWord();
}

TEST(textProcessorTest,TestMultithreadedExample)
{
    textProcessorTest::TestMultithreadedExample();
}

TEST(textProcessorTest,TestHelpReverse)
{
    textProcessorTest::TestHelpReverse();
}

TEST(textProcessorTest,TestHelpFindSeq)
{
    textProcessorTest::TestHelpFindSeq();
}

TEST(textProcessorTest,TestOccurences)
{
    textProcessorTest::TestOccurences();
}

TEST(textProcessorTest,TestMaxElement)
{
    textProcessorTest::TestMaxElement();
}

#endif //TEXTPROCESSOR_TEXTPROCESSORTEST_H
