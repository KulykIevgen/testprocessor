# How to build the project on Windows.

* Install [cmake](https://github.com/Kitware/CMake/releases/download/v3.14.0/cmake-3.14.0-win64-x64.msi), [python3](https://www.python.org/ftp/python/3.7.3/python-3.7.3-amd64.exe), [Visual Studio 2017](https://visualstudio.microsoft.com/ru/thank-you-downloading-visual-studio/?sku=Community&rel=15) and [Doxygen](https://sourceforge.net/projects/doxygen/files/latest/download)
* `mkdir build`
* `cd build`
* `cmake .. -G "Visual Studio 15"`
* `cmake --build . --target ALL_BUILD --config RelWithDebInfo`
* `ctest -VV -C RelWithDebInfo`

# How to generate documentation

* first install doxygen
* run `doxygen .\Doxyfile.txt`
* find documention in .\docs directory

# Appveyor CI
[![Build status](https://ci.appveyor.com/api/projects/status/mc0cq2uyus2bg35h?svg=true)](https://ci.appveyor.com/project/KulykIevgen/testprocessor)